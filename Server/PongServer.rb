#!/usr/bin/env ruby

require 'em-websocket'


EM.run {
  EM::WebSocket.run(:host => "0.0.0.0", :port => 8080 ) do |ws|
    ws.onopen{ |handshake|
      ws.send "Hello Client, you are connected to #{handshake.path}"
    }

    ws.onmessage { |msg|
      puts "Recieved message: #{msg}"
      ws.send "Pong: #{msg}"
    }

    ws.onclose{
      puts 'Connection closed.'
    }
  end
}