Simple Pong
============
This Pong was written as to be used for the demonstration and workings
of Git and GitLab.

Please note that JavaScript is NOT my language of choice, but I chose
this for this demonstration, as I suspect that all developers are able
to at least make some sense fo what is going on


Features to be added
--------------------
As part of the demonstration, the following features, or as many as
time permits will be implemented. And in doing so, it is my hope that
the use of Git and GitLab will show their advantages.

* Ball, Score sounds.
* Score-board.
* Winder announcement, reset game.
* Two-player. 
* Pause/Unpause.
* Walls.
* Gifts, containing paddle-size changes.