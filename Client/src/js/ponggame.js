goog.provide('pongApp.start');
goog.require('dk.krakow.Ajax');
goog.require('dk.krakow.Font');
goog.require('dk.krakow.PongGame');

pongApp.start = function() {
    new dk.krakow.PongGame();
};