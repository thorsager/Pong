goog.provide('dk.krakow.PongGame');
goog.require('dk.krakow.font.BasicFont');

dk.krakow.PongGame = ( function(){
    var WIDTH = 700, HEIGHT = 600;
    var upArrow = 38, downArrow = 40, spaceBar=32;
    var fontSize = 4;

    return function() {
        var fontManager = new dk.krakow.Font( new dk.krakow.font.BasicFont,fontSize );
        var canvas, ctx, keyState, gameMode=-1;
        var player1, player2, ball;


        player1 = {
            x: null,
            y: null,
            width: 20,
            height: 100,
            score: 0,

            /**
             * Update function for the player Object
             */
            update: function () {
                if (keyState[upArrow]) this.y -= 10;
                if (keyState[downArrow]) this.y += 10;
                this.y = Math.max(Math.min(this.y, HEIGHT - this.height), 0);
            },

            /**
             * Draw function for the draw Object
             */
            draw: function () {
                ctx.fillRect(this.x, this.y, this.width, this.height);
            }
        };

        player2 = {
            x: null,
            y: null,
            width: 20,
            height: 100,
            score: 0,

            update: function () {
                // this is an AI update function
                var dest = ball.y - ( this.height - ball.side ) * 0.5;
                this.y += (dest - this.y) * 0.1;
                this.y = Math.max(Math.min(this.y, HEIGHT - this.height), 0);
            },

            draw: function () {
                ctx.fillRect(this.x, this.y, this.width, this.height);
            }
        };

        ball = {
            x: null,
            y: null,
            vel: null,
            speed: 12,
            side: 15,
            wall: new Audio('resources/snd/pong_wall.mp3'),
            pong: new Audio('resources/snd/pong_paddle.mp3'),
            score: new Audio('resources/snd/pong_score.mp3'),

            serve: function (direction) {
                var r = Math.random();
                this.x = direction === 1 ? player1.x + player1.width : player2.x - this.side;
                this.y = (HEIGHT - this.side) * r;

                var phi = 0.1 * Math.PI * (1 - 2 * r);
                this.vel = {
                    x: direction * this.speed * Math.cos(phi),
                    y: this.speed * Math.sin(phi)
                }


            },

            update: function () {
                this.x += this.vel.x;
                this.y += this.vel.y;

                if (this.y < 0 || this.y + this.side > HEIGHT) {
                    var offset = this.vel.y < 0 ? 0 - this.y : HEIGHT - (this.y + this.side); // offset
                    this.y += offset * 2;
                    this.vel.y *= -1;
                    ball.wall.play();
                }

                var BoxIntersect = function (ax, ay, aw, ah, bx, by, bw, bh) {
                    return ax < bx + bw && ay < by + bh && bx < ax + aw && by < ay + ah;
                };

                var p = this.vel.x < 0 ? player1 : player2;
                if (BoxIntersect(p.x, p.y, p.width, p.height, this.x, this.y, this.side, this.side)) {
                    this.x = p === player1 ? player1.x + player1.width : player2.x - this.side;
                    var n = (this.y + this.side - p.y) / (p.height + this.side);
                    var phi = 0.25 * Math.PI * (2 * n - 1); // pi/4 = 45


                    var smash = Math.abs(phi) > 0.2 * Math.PI ? 1.5 : 1;

                    this.vel.x = smash * (p === player1 ? 1 : -1) * this.speed * Math.cos(phi);
                    this.vel.y = smash * this.speed * Math.sin(phi);
                    ball.pong.play();
                }

                // Check if ball is out of bounds (meaning that.. SCORE!!)
                if (this.x + this.side < 0 || this.x > WIDTH) {
                    score(ball,p === player1 ? player2 :player1,p);
                    this.serve(p === player1 ? 1 : -1)
                }

            },

            draw: function () {
                ctx.fillRect(this.x, this.y, this.side, this.side);
            }
        };

        function score(ball,winner,looser) {
            ball.score.play();
            winner.score++;
            if (looser.height > 50) looser.height -= 5;
            if (winner.height < 150) winner.height += 5;
        }

        function main() {
            canvas = document.createElement("canvas");
            canvas.width = WIDTH;
            canvas.height = HEIGHT;
            ctx = canvas.getContext("2d");
            document.body.appendChild(canvas);

            keyState = {};
            document.addEventListener("keydown", function (evt) {
                keyState[evt.keyCode] = true;
                if (evt.keyCode===spaceBar) {
                    if ( gameMode === -1) {
                        gameMode = 1;
                    } else {
                        gameMode = 1-gameMode;
                    }
                }
            });
            document.addEventListener("keyup", function (evt) {
                delete keyState[evt.keyCode];
            });

            canvas.addEventListener("mousemove",function(evt) {
                if (gameMode == 1) {
                    player1.y = evt.offsetY - player1.height*0.5;
                }
                console.log(evt);
            });

            init();

            var loop = function () {
                update();
                draw();
                window.requestAnimationFrame(loop, canvas);
            };
            window.requestAnimationFrame(loop, canvas);

        }

        /**
         * Object Initialization.
         */
        function init() {
            // X offset player with the width of the player, Y to the middle of canvas
            player1.x = player1.width;
            player1.y = (HEIGHT - player1.height) * 0.5;
            player2.x = WIDTH - (player1.width + player2.width);
            player2.y = (HEIGHT - player2.height) * 0.5;
            ball.serve(1);
        }

        /**
         * Global update function.
         * Will update all objects.
         */
        function update() {
            if (gameMode === 1) { // running
                ball.update();
                player1.update();
                player2.update();
            }
        }

        /**
         * Global draw function.
         * Will draw all objects.
         */
        function draw() {
            ctx.fillRect(0, 0, WIDTH, HEIGHT);

            ctx.save();

            ctx.fillStyle = "#fff";

            ball.draw();
            player1.draw();
            player2.draw();
            var pl1x = (WIDTH * 0.5) - fontManager.width(player1.score) - (fontSize*3) ;
            var pl2x = (WIDTH * 0.5) + fontSize* 2;

            fontManager.draw(ctx, pl1x, 20, player1.score);
            fontManager.draw(ctx, pl2x, 20, player2.score);

            switch (gameMode) {
                case -1:
                    fontManager.draw(ctx,
                        WIDTH*0.5-fontManager.width("ready player one")*0.5,
                        HEIGHT*0.5-fontSize*0.5, "ready player one");
                    break;
                case 0:
                    fontManager.draw(ctx,
                        WIDTH*0.5-fontManager.width("pause")*0.5,
                        HEIGHT*0.5-fontSize*0.5, "pause");
                    break;
            }

            // draw the net
            var w = 4;
            var x = (WIDTH - w) * 0.5;
            var y = 0;
            var step = HEIGHT / 20;

            while (y < HEIGHT) {
                ctx.fillRect(x, y + step * 0.25, w, step * 0.5);
                y += step;
            }

            ctx.restore();
        }

        main();
    }

})();
