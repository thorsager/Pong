goog.provide('dk.krakow.Ajax');
dk.krakow.Ajax = function() {};
dk.krakow.Ajax.GET = function(url,onSuccess,onError) {
    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status === 200){
            onSuccess(xmlhttp.responseText);
        } else if (xmlhttp.readyState == 4 && xmlhttp.status !== 300) {
            onError(xmlhttp.readyState, xmlhttp.status, xmlhttp.responseText)
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
};