goog.provide('dk.krakow.Font');
dk.krakow.Font = (function(){

    return function (fnt,size) {
        var font = fnt;
        var scale = size;

        function blockToXY(i) {
            var x, y;
            y = Math.floor(i / font.height());
            x = i % font.width();
            return {'x': x, 'y': y, 'i': i};
        }

        function drawChar(ctx,x, y, character) {
            for (var i = 0; i < font.charData(character).length; i++) {
                var b = blockToXY(i);
                var x1 = b.x * scale + x;
                var y1 = b.y * scale + y;
                if (font.charData(character)[i] === 1) {
                    ctx.beginPath();
                    ctx.fillRect(x1, y1, scale, scale);
                    ctx.fill();
                }
            }
        }

        // public
        this.draw = function (ctx, x, y, str) {
            var s = ''+str;
            for (var i = 0; i < s.length; i++) {
                drawChar(ctx, x + (i * font.width() * scale), y, s.charAt(i));
            }
        };

        this.width = function (str) {
            var s = ''+str;
            return (s.length)*(font.width()*scale);
        };

    };
})();
