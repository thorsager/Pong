goog.provide('dk.krakow.FontEditor');
dk.krakow.FontEditor = (function () {

    /**
     * Actual font width in pixels
     * @type {number}
     */
    var FONT_WIDTH = 8;

    /**
     * Actual font height in pixels
     * @type {number}
     */
    var FONT_HEIGHT = 8;

    /**
     * Editor font-pixel to block scale
     * @type {number}
     */
    var SCALE = 35;

    /**
     * Width of the line drawn between blocks
     * @type {number}
     */
    var LINE_WIDTH = 1;

    /**
     * The width of the canvas used.
     * Calculated from font-size, scale and line-width
     * @type {number}
     */
    var WIDTH = FONT_WIDTH * SCALE + ((FONT_WIDTH + 1) * LINE_WIDTH);
    /**
     * Thew height of the canvas used.
     * Calculated from font-size, scale and line-width
     * @type {number}
     */
    var HEIGHT = FONT_HEIGHT * SCALE + ((FONT_HEIGHT + 1) * LINE_WIDTH);

    return function (canvasId, controlsId) {
        var canvas = document.getElementById(canvasId);
        var ctx = canvas.getContext('2d');

        var blockUnderCursor = null;
        var mousePressed = false;
        var currentChar = 0;

        var controls = document.getElementById(controlsId);
        var table = null;
        var download = null;
        var upload = null;

        var blocks = [];
        var font = {
            0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [],
            a: [], b: [], c: [], d: [], e: [], f: [], g: [], h: [], i: [], j: [],
            k: [], l: [], m: [], n: [], o: [], p: [], q: [], r: [], s: [], t: [],
            u: [], v: [], w: [], x: [], y: [], z: []
        };

        init();

        function init() {
            // initialize blocks array
            for (var i = 0; i < (FONT_WIDTH * FONT_HEIGHT); i++) {
                blocks[i] = {'topLeft': null};
            }

            // Setup canvas height
            canvas.height = HEIGHT;
            canvas.width = WIDTH;

            // Add mouse envent-handlers on canvas
            canvas.addEventListener('mousedown', onMouseDown, false);
            canvas.addEventListener('mouseup', onMouseUp, false);
            canvas.addEventListener('mousemove', onMouseMove, false);
            canvas.addEventListener('mouseout', onMouseOut, false);

            // add table
            table = document.createElement('div');
            //table = document.createElement('div');
            table.setAttribute('id', 'table');
            controls.appendChild(table);

            // add upload control
            upload = document.createElement('input');
            upload.setAttribute('id', 'upload');
            upload.setAttribute('type', 'file');
            upload.setAttribute('accept', '.jsfont');
            upload.addEventListener('change', uploadFont, false);
            controls.appendChild(upload);

            // add download control
            download = document.createElement('a');
            download.setAttribute('id', 'download');
            download.innerHTML = 'Download';
            download.addEventListener('click', downloadFont, false);
            controls.appendChild(download);

            initTable();

            startAnimationLoop();
        }

        /**
         * Star animation loop
         */
        function startAnimationLoop() {
            var loop = function () {
                draw();
                window.requestAnimationFrame(loop, canvas);
            };
            window.requestAnimationFrame(loop, canvas);
        }

        /**
         * Set up the "char" table and draw it
         */
        function initTable() {
            for (var key in font) {
                if (font.hasOwnProperty(key)) {
                    font[key] = [];
                    for (var i = 0; i < FONT_WIDTH * FONT_HEIGHT; i++) {
                        font[key][i] = 0;
                    }
                    var div = document.createElement('div');
                    div.setAttribute('id', 'val_' + key);
                    //div.innerHTML = key + ': ' + blocksAsBitStr(font[key]);
                    div.addEventListener('click', (function () {
                        var ck = key;
                        return function () {
                            currentChar = ck;
                            updateTable();
                        }
                    })(), false);
                    table.appendChild(div);
                }
            }
            updateTable();
        }

        /**
         * Handle mouse Down events
         * @param e
         */
        function onMouseDown(e) {
            flipBlock(blockUnderCursor);
            updateTable();
            mousePressed = true;
        }

        /**
         * Handle mouse Up events
         * @param e
         */
        function onMouseUp(e) {
            mousePressed = false;
        }

        /**
         * Handle mouse move evnets
         * @param e
         */
        function onMouseMove(e) {
            var newBlock = locateBlock(e.offsetX, e.offsetY);
            if (newBlock !== blockUnderCursor) {
                blockUnderCursor = newBlock;
                console.log("Block:" + blockUnderCursor + " :: " + mousePressed);
                if (mousePressed) {
                    flipBlock(blockUnderCursor);
                    updateTable();
                }
            }
        }

        /**
         * Handle mous "out" events (when mouse leaves canvas)
         * @param e
         */
        function onMouseOut(e) {
            mousePressed = false;
        }

        /**
         * Download font from font-buffer to file..
         * @param e
         */
        function downloadFont(e) {
            var target = e.target;
            target.setAttribute('href', 'data:application/json;charset=utf-8,' + JSON.stringify(font));
            target.setAttribute('download', 'font.jsfont');
        }

        /**
         * Upload font from file to font-buffer..
         * @param e
         */
        function uploadFont(e) {
            var file = e.target.files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    font = JSON.parse(e.target.result);
                    updateTable();
                };
                reader.readAsText(file);
            }
        }

        /**
         * Main draw function
         * Called from the animation loop
         */
        function draw() {
            drawGrid();
            drawBlocks();
        }


        /**
         * Draws grid around blocks
         */
        function drawGrid() {
            var i, j, o;

            function line(x, y, x1, y1) {
                ctx.moveTo(x, y);
                ctx.lineTo(x1, y1);
            }

            ctx.beginPath();
            ctx.lineWidth = LINE_WIDTH;
            o = LINE_WIDTH * 0.5;
            for (i = 0; i <= FONT_WIDTH; i++) {
                line((i * SCALE + i * LINE_WIDTH) + o, o, (i * SCALE + i * LINE_WIDTH) + o, HEIGHT + o);
                for (j = 0; j <= FONT_HEIGHT; j++) {
                    line(0, (j * SCALE + j * LINE_WIDTH) + o, WIDTH + o, (j * SCALE + j * LINE_WIDTH) + o);
                }
            }
            ctx.stroke();
        }

        /**
         * Draw the blocks
         */
        function drawBlocks() {
            var i;
            for (i = 0; i < blocks.length; i++) {
                if (blocks[i].topLeft === null) {
                    var b = blockToXY(i);
                    blocks[i].topLeft = blockLocation(b.x, b.y);
                }
                ctx.beginPath();
                ctx.rect(blocks[i].topLeft.x, blocks[i].topLeft.y, SCALE, SCALE);
                ctx.fillStyle = font[currentChar][i] === 1 ? 'green' : 'blue';
                ctx.fill();
            }
        }

        /**
         * Update table
         */
        function updateTable() {
            for (var key in font) {
                if (font.hasOwnProperty(key)) {
                    var e = document.getElementById('val_' + key);
                    if ( typeof e === 'undefined') {
                        e = document.createElement('div');
                        e.setAttribute('id','val_'+key);
                        table.appendChild(e);
                    }
                    e.innerHTML = key+': '+blocksAsBitStr(font[key]);
                    if (key == currentChar) {
                        e.className = 'active';
                    } else {
                        e.className = '';
                    }
                }
            }
        }

        /**
         * Dump font buffer "char" to bit-string
         * @param buffer
         * @returns {string}
         */
        function blocksAsBitStr(buffer) {
            var i;
            var str = '';
            for (i = 0; i < buffer.length; i++) {
                if (i > 0 && i % FONT_WIDTH == 0) {
                    str += ':';
                }
                str += buffer[i];
            }
            return str;
        }

        /**
         * Calculate that actual x,y pixel coordinates on the canvas for a block.
         * @param x block x coordinate
         * @param y block y coordinate
         * @returns {{x: (number|*), y: (number|*)}}
         */
        function blockLocation(x, y) {
            var x1, y1;
            x1 = (x * SCALE + x * LINE_WIDTH) + LINE_WIDTH;
            y1 = (y * SCALE + y * LINE_WIDTH) + LINE_WIDTH;
            return {'x': x1, 'y': y1};
        }

        /**
         * Calculate the block x,y coordinate from actual block id
         * @param i block id (in buffer)
         * @returns {{x: (number|*), y: (number|*), i: *}}
         */
        function blockToXY(i) {
            var x, y;
            y = Math.floor(i / FONT_HEIGHT);
            x = i % FONT_WIDTH;
            return {'x': x, 'y': y, 'i': i};
        }

        /**
         * Flip the bit value of the block in buffer
         * @param bid block id
         */
        function flipBlock(bid) {
            if (bid !== null) {
                font[currentChar][bid] = font[currentChar][bid] === 1 ? 0 : 1;
            }
        }

        /**
         * Find block from actual x,y pixel coordinates on the canvas
         * @param x coordinate on the canvas
         * @param y coordinate on the canvas
         * @returns {*} blockId or null if not found.
         */
        function locateBlock(x, y) {
            var i;
            for (i = 0; i < blocks.length; i++) {
                var tl = blocks[i].topLeft;
                if ((x >= tl.x && x <= tl.x + SCALE) && (y >= tl.y && y <= tl.y + SCALE)) {
                    return i;
                }
            }
            return null;
        }

        //public
    };
})();