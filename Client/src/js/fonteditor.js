goog.provide('fontEditorApp.start');
goog.require('dk.krakow.FontEditor');

fontEditorApp.start = function(a,b) {
    new dk.krakow.FontEditor(a,b);
};