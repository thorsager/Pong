#!/usr/bin/env bash

BASE=`dirname $0`
RCFILE="$BASE/projectrc"

if [ -e $RCFILE ]; then
	. $RCFILE
fi


function check_closure_lib() {
	echo -n "Checking for closure-library.."
	if [ ! -d "$CLOSURE_LIB" ]; then
		echo " not found (cloning)."
		git clone $CLOSURE_GIT_REPO $CLOSURE_LIB
	else
		echo " found."
	fi;
}

function build_editor() {
	echo -n "Compiling 'editor'.."
    java -jar "$BASE/$CLOSURE_COMPILER" \
    	--js_output_file=src/js/fonteditor.compiled.js \
     	--language_in ECMASCRIPT5_STRICT \
     	--closure_entry_point 'fontEditorApp.start' \
     	--js "$BASE/$JAVA_SCRIPT"\
     	--warning_level=VERBOSE \
     	--only_closure_dependencies \
        --manage_closure_dependencies
#     	--js "$BASE/$CLOSURE_LIB/**.js" \
#        --compilation_level ADVANCED_OPTIMIZATIONS
#     	--debug \
#     	--js 'src/js/dk/**.js' \
#     	--js 'src/js/fonteditor.js'  \
	echo " done."
}



function build_pong() {
	echo -n "Compiling 'pong'.."
	java -jar lib/compiler.jar \
    	--js_output_file=src/js/pong.compiled.js \
     	--language_in ECMASCRIPT5_STRICT \
     	--only_closure_dependencies \
     	--closure_entry_point 'pongApp.start' \
     	--js 'src/js/dk/**.js' \
     	--js 'src/js/ponggame.js'  \
     	--warning_level=VERBOSE \
#    	--js 'lib/closure-library/**.js' \
#     --manage_closure_dependencies \
#     --common_js_entry_module 'src/js/fonteditor.js' \
#     --js 'lib/closure-library/**.js' \
	echo " done."
}

case "$1" in
    editor)
		check_closure_lib;
        build_editor;
    ;;
    pong)
		check_closure_lib
        build_pong;
    ;;
    *)
        echo "USAGE: `basename $0`: <editor|pong>"
    ;;
esac
